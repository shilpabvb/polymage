#include "detection_layer.h"
#include "activations.h"
#include "softmax_layer.h"
#include "blas.h"
#include "box.h"
#include "cuda.h"
#include "utils.h"

#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#define T 32
detection_layer make_detection_layer(int batch, int inputs, int n, int side, int classes, int coords, int rescore)
{
    detection_layer l = {0};
    l.type = DETECTION;

    l.n = n;
    l.batch = batch;
    l.inputs = inputs;
    l.classes = classes;
    l.coords = coords;
    l.rescore = rescore;
    l.side = side;
    l.w = side;
    l.h = side;
    assert(side*side*((1 + l.coords)*l.n + l.classes) == inputs);
    l.cost = calloc(1, sizeof(float));
    l.outputs = l.inputs;
    l.truths = l.side*l.side*(1+l.coords+l.classes);
    l.output = calloc(batch*l.outputs, sizeof(float));
    l.delta = calloc(batch*l.outputs, sizeof(float));

    l.forward = forward_detection_layer;
    l.backward = backward_detection_layer;
#ifdef GPU
    l.forward_gpu = forward_detection_layer_gpu;
    l.backward_gpu = backward_detection_layer_gpu;
    l.output_gpu = cuda_make_array(l.output, batch*l.outputs);
    l.delta_gpu = cuda_make_array(l.delta, batch*l.outputs);
#endif

    fprintf(stderr, "Detection Layer\n");
    srand(0);

    return l;
}

void forward_detection_layer(const detection_layer l, network net)
{
    int locations = l.side*l.side;
    int i,j;
    memcpy(l.output, net.input, l.outputs*l.batch*sizeof(float));
    //if(l.reorg) reorg(l.output, l.w*l.h, size*l.n, l.batch, 1);
    int b, ii, jj, bb;
    if (l.softmax){
	#pragma omp parallel for schedule(static)
        for(b = 0; b < l.batch/T; ++b)
	{          
		for(bb=T*b; bb<(T*b+T);bb++)
		{
            for (i = 0; i < locations/T; ++i) 
	    {
	    	   #pragma simd
	    	   #pragma always vectorize
		   #pragma ivdep
		   for(ii=T*i; ii<(T*i+T);ii++)
		   {
		   	int index = bb*l.inputs;
                	int offset = ii*l.classes;
                	softmax(l.output + index + offset, l.classes, 1, 1,l.output + index + offset);
		   }
               }
            }
        }
    }
    if(net.train){
        float avg_iou = 0;
        float avg_cat = 0;
        float avg_allcat = 0;
        float avg_obj = 0;
        float avg_anyobj = 0;
        int count = 0;
        *(l.cost) = 0;
        int size = l.inputs * l.batch;
	int best_index;
        float best_iou;
        float best_rmse;
	int truth_index;

	box truth;
        memset(l.delta, 0, size * sizeof(float));
	
	#pragma omp parallel for schedule(static)
        for (b = 0; b < l.batch/T; ++b)
	{
	//	#pragma omp parallel for
		for(bb=T*b; bb<(T*b+T);bb++)
		{
            for (i = 0; i < locations/T; ++i) 
	    {
	    	   //#pragma simd
	    	   //#pragma always vectorize
		   for(ii=T*i; ii<(T*i+T);ii++)
		   {
            		int index = bb*l.inputs;
	               	truth_index = (bb*locations + ii)*(1+l.coords+l.classes);
                	int is_obj = net.truth[truth_index];
		   
			//#pragma omp parallel for
                	for (j = 0; j < l.n/T; ++j) 
			{
	    		#pragma simd
	    		#pragma always vectorize
			#pragma ivdep
		   		for(jj=T*j; jj<(T*j+T);jj++)
		   		{
                    			int p_index = index + locations*l.classes + ii*l.n + jj;
                    			l.delta[p_index] = l.noobject_scale*(0 - l.output[p_index]);
                    			*(l.cost) += l.noobject_scale*pow(l.output[p_index], 2);
                    			avg_anyobj += l.output[p_index];
				}
			}
                    	
			best_index = -1;
                	best_iou = 0;
                	best_rmse = 20;
                	if (!is_obj){
                    		continue;
                	}
		 }
		}
	       }
             }

	#pragma omp parallel for schedule(static)
        for (b = 0; b < l.batch/T; ++b)
	{
	//	#pragma omp parallel for
		for(bb=T*b; bb<(T*b+T);bb++)
		{
            for (i = 0; i < locations/T; ++i) 
	    {
	    	   //#pragma simd
	    	   //#pragma always vectorize
		   for(ii=T*i; ii<(T*i+T);ii++)
		   {
	               	truth_index = (bb*locations + ii)*(1+l.coords+l.classes);
                	int class_index = index + ii*l.classes;
  	    		//#pragma omp parallel for
                	for(j = 0; j < l.classes/T; ++j) 
			{
	 		#pragma simd
	    		#pragma always vectorize
			#pragma ivdep
		   		for(jj=T*j; jj<(T*j+T);jj++)
		   		{
		                    l.delta[class_index+jj] = l.class_scale * (net.truth[truth_index+1+jj] - l.output[class_index+jj]);
		                    *(l.cost) += l.class_scale * pow(net.truth[truth_index+1+jj] - l.output[class_index+jj], 2);
                    		    if(net.truth[truth_index + 1 + jj]) avg_cat += l.output[class_index+jj];
                    		    avg_allcat += l.output[class_index+jj];
                		}
			}
                	truth = float_to_box(net.truth + truth_index + 1 + l.classes, 1);
                	truth.x /= l.side;
                	truth.y /= l.side;
		   }
               }
             }
          }
		
	#pragma omp parallel for schedule(static)
        for (b = 0; b < l.batch/T; ++b)
	{
	//	#pragma omp parallel for
		for(bb=T*b; bb<(T*b+T);bb++)
		{
            for (i = 0; i < locations/T; ++i) 
	    {
	    	   //#pragma simd
	    	   //#pragma always vectorize
		   for(ii=T*i; ii<(T*i+T);ii++)
		   {
			//#pragma omp parallel for
                	for(j = 0; j < l.n/T; ++j)
			{
	    		#pragma simd
	    		#pragma always vectorize
			#pragma ivdep
		   		for(jj=T*j; jj<(T*j+T);jj++)
		   		{
                    			int box_index = index + locations*(l.classes + l.n) + (i*l.n + jj) * l.coords;
                    			box out = float_to_box(l.output + box_index, 1);
                    			out.x /= l.side;
                    			out.y /= l.side;
				        if (l.sqrt)
					{
                        			out.w = out.w*out.w;
                        			out.h = out.h*out.h;
                    			}
			                float iou  = box_iou(out, truth);
                    			//iou = 0;
                    			float rmse = box_rmse(out, truth);
                    			if(best_iou > 0 || iou > 0)
					{
                        			if(iou > best_iou)
						{
                            				best_iou = iou;
                            				best_index = jj;
                        			}
                    			}
					else
					{
                        			if(rmse < best_rmse)
						{
                            				best_rmse = rmse;
                           	 			best_index = jj;
                        			}
                    			}
                		}
			}
		     }
                  }
                }
              }
                	if(l.forced)
			{
                    		if(truth.w*truth.h < .1)
				{
                        		best_index = 1;
                    		}
				else
				{
                        		best_index = 0;
                    		}
                	}
                	if(l.random && *(net.seen) < 64000)
			{
                    		best_index = rand()%l.n;
                	}

                	int box_index = index + locations*(l.classes + l.n) + (ii*l.n + best_index) * l.coords;
                	int tbox_index = truth_index + 1 + l.classes;

                	box out = float_to_box(l.output + box_index, 1);
                	out.x /= l.side;
                	out.y /= l.side;
                	if (l.sqrt) 
			{
                    		out.w = out.w*out.w;
                    		out.h = out.h*out.h;
                	}
                	float iou  = box_iou(out, truth);

                	//printf("%d,", best_index);
                	int p_index = index + locations*l.classes + ii*l.n + best_index;
                	*(l.cost) -= l.noobject_scale * pow(l.output[p_index], 2);
                	*(l.cost) += l.object_scale * pow(1-l.output[p_index], 2);
                	avg_obj += l.output[p_index];
                	l.delta[p_index] = l.object_scale * (1.-l.output[p_index]);

                	if(l.rescore){
                    		l.delta[p_index] = l.object_scale * (iou - l.output[p_index]);
                	}

                	l.delta[box_index+0] = l.coord_scale*(net.truth[tbox_index + 0] - l.output[box_index + 0]);
                	l.delta[box_index+1] = l.coord_scale*(net.truth[tbox_index + 1] - l.output[box_index + 1]);
                	l.delta[box_index+2] = l.coord_scale*(net.truth[tbox_index + 2] - l.output[box_index + 2]);
                	l.delta[box_index+3] = l.coord_scale*(net.truth[tbox_index + 3] - l.output[box_index + 3]);
               	 	if(l.sqrt){
                    		l.delta[box_index+2] = l.coord_scale*(sqrt(net.truth[tbox_index + 2]) - l.output[box_index + 2]);
                    		l.delta[box_index+3] = l.coord_scale*(sqrt(net.truth[tbox_index + 3]) - l.output[box_index + 3]);
                	}

                	*(l.cost) += pow(1-iou, 2);
                	avg_iou += iou;
                	++count;
          	
        if(0){
            float *costs = calloc(l.batch*locations*l.n, sizeof(float));
	    #pragma omp parallel for
            for (b = 0; b < l.batch; ++b) {
                int index = b*l.inputs;
	    //#pragma omp parallel for
	    #pragma simd
	    #pragma always vectorize
                for (i = 0; i < locations; ++i) {
                    for (j = 0; j < l.n; ++j) {
                        int p_index = index + locations*l.classes + i*l.n + j;
                        costs[b*locations*l.n + i*l.n + j] = l.delta[p_index]*l.delta[p_index];
                    }
                }
            }
            int indexes[100];
            top_k(costs, l.batch*locations*l.n, 100, indexes);
            float cutoff = costs[indexes[99]];
	    #pragma omp parallel for
            for (b = 0; b < l.batch; ++b) {
                int index = b*l.inputs;
	    #pragma simd
	    #pragma always vectorize
	    //#pragma omp parallel for
                for (i = 0; i < locations; ++i) {
                    for (j = 0; j < l.n; ++j) {
                        int p_index = index + locations*l.classes + i*l.n + j;
                        if (l.delta[p_index]*l.delta[p_index] < cutoff) l.delta[p_index] = 0;
                    }
                }
            }
            free(costs);
        }


        *(l.cost) = pow(mag_array(l.delta, l.outputs * l.batch), 2);


        printf("Detection Avg IOU: %f, Pos Cat: %f, All Cat: %f, Pos Obj: %f, Any Obj: %f, count: %d\n", avg_iou/count, avg_cat/count, avg_allcat/(count*l.classes), avg_obj/count, avg_anyobj/(l.batch*locations*l.n), count);
        //if(l.reorg) reorg(l.delta, l.w*l.h, size*l.n, l.batch, 0);
    }
}

void backward_detection_layer(const detection_layer l, network net)
{
    //axpy_cpu(l.batch*l.inputs, 1, l.delta, 1, net.delta, 1);
    daxpy_(l.batch*l.inputs, 1, l.delta, 1, net.delta, 1);    
}

void get_detection_boxes(layer l, int w, int h, float thresh, float **probs, box *boxes, int only_objectness)
{
    int i,j,n;
    float *predictions = l.output;
    //int per_cell = 5*num+classes;
    #pragma omp parallel for schedule(static)
    for (i = 0; i < l.side*l.side; ++i){
        int row = i / l.side;
        int col = i % l.side;
        for(n = 0; n < l.n; ++n){
            int index = i*l.n + n;
            int p_index = l.side*l.side*l.classes + i*l.n + n;
            float scale = predictions[p_index];
            int box_index = l.side*l.side*(l.classes + l.n) + (i*l.n + n)*4;
            boxes[index].x = (predictions[box_index + 0] + col) / l.side * w;
            boxes[index].y = (predictions[box_index + 1] + row) / l.side * h;
            boxes[index].w = pow(predictions[box_index + 2], (l.sqrt?2:1)) * w;
            boxes[index].h = pow(predictions[box_index + 3], (l.sqrt?2:1)) * h;
            for(j = 0; j < l.classes; ++j){
                int class_index = i*l.classes;
                float prob = scale*predictions[class_index+j];
                probs[index][j] = (prob > thresh) ? prob : 0;
            }
            if(only_objectness){
                probs[index][0] = scale;
            }
        }
    }
}

#ifdef GPU

void forward_detection_layer_gpu(const detection_layer l, network net)
{
    if(!net.train){
        copy_gpu(l.batch*l.inputs, net.input_gpu, 1, l.output_gpu, 1);
        return;
    }

    //float *in_cpu = calloc(l.batch*l.inputs, sizeof(float));
    //float *truth_cpu = 0;

    forward_detection_layer(l, net);
    cuda_push_array(l.output_gpu, l.output, l.batch*l.outputs);
    cuda_push_array(l.delta_gpu, l.delta, l.batch*l.inputs);
}

void backward_detection_layer_gpu(detection_layer l, network net)
{
    axpy_gpu(l.batch*l.inputs, 1, l.delta_gpu, 1, net.delta_gpu, 1);
    //copy_gpu(l.batch*l.inputs, l.delta_gpu, 1, net.delta_gpu, 1);
}
#endif

